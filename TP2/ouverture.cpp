// ouverture.cpp enchaine une érosion et une dilatation pour supprimer les taches du fond d'une image binaire

#include <stdio.h>
#include "image_ppm.h"



int main(int argc, char const *argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;

    
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut, *ImgOut_temp;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut_temp, OCTET, nTaille);
   allocation_tableau(ImgOut, OCTET, nTaille);

	
   for (int i=0; i < nTaille; i++){
    ImgOut_temp[i]=ImgIn[i];
   }



   for (int i=0; i < nH; i++){
     for (int j=0; j < nW; j++){
      int n = i*nW+j;
      if (ImgIn[n]==255)
      {
        if(j!=0) ImgOut_temp[n-1]=255;
        if(j!=nW-1) ImgOut_temp[n+1]=255;
        if(i!=0) ImgOut_temp[n-nW]=255;
        if(i!=nH-1) ImgOut_temp[n+nW]=255;
      }
     }
   }

   for (int i=0; i < nTaille; i++){
    ImgOut[i]=ImgOut_temp[i];
   }

   for (int i=0; i < nH; i++){
     for (int j=0; j < nW; j++){
      int n = i*nW+j;
      if (ImgOut_temp[n]==0)
      {
        if(j!=0) ImgOut[n-1]=0;
        if(j!=nW-1) ImgOut[n+1]=0;
        if(i!=0) ImgOut[n-nW]=0;
        if(i!=nH-1) ImgOut[n+nW]=0;
      }
     }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut); free(ImgOut_temp);

   return 1;
 }