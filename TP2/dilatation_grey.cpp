// dilatation_grey.cpp : augmente la taille de l'objet d'une image en niveaux de gris

#include <stdio.h>
#include "image_ppm.h"



int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;

    
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   
   for (int i=0; i < nH; i++){
     for (int j=0; j < nW; j++){
      int n = i*nW+j;
      int min=ImgIn[n];
      if(j!=0 && ImgIn[n-1]<min) min=ImgIn[n-1];
      if(j!=nW-1 && ImgIn[n+1]<min) min=ImgIn[n+1];
      if(i!=0 && ImgIn[n-nW]<min) min=ImgIn[n-nW];
      if(i!=nH-1 && ImgIn[n+nW]<min) min=ImgIn[n+nW];
      ImgOut[n]=min;
     }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}