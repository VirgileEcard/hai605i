// fermeture_couleur.cpp : applique la fermeture à une image .ppm

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);


   OCTET *ImgIn, *ImgOut, *ImgOut2;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);
   allocation_tableau(ImgOut2, OCTET, nTaille3);


   for (int i=1; i < nH; i++){
     for (int j=0; j < 3*nW; j+=3){
      int n = (i*3*nW)+j;
      int minR=ImgIn[n];
      int minG=ImgIn[n+1];
      int minB=ImgIn[n+2];

      if(j!=0){
        if(ImgIn[n-3]<minR) minR=ImgIn[n-3];
        if(ImgIn[n-2]<minG) minG=ImgIn[n-2];
        if(ImgIn[n-1]<minB) minB=ImgIn[n-1];
      }

      if(j!=(3*nW)-3){
        if(ImgIn[n+3]<minR) minR=ImgIn[n+3];
        if(ImgIn[n+4]<minG) minG=ImgIn[n+4];
        if(ImgIn[n+5]<minB) minB=ImgIn[n+5];
      }

      if(i!=0){
        if(ImgIn[n-(3*nW)]<minR) minR=ImgIn[n-(3*nW)];
        if(ImgIn[n-(3*nW)+1]<minG) minG=ImgIn[n-(3*nW)+1];
        if(ImgIn[n-(3*nW)+2]<minB) minB=ImgIn[n-(3*nW)+2];      
      }

      if(i!=nH-1){
        if(ImgIn[n+(3*nW)]<minR) minR=ImgIn[n+(3*nW)];
        if(ImgIn[n+(3*nW)+1]<minG) minG=ImgIn[n+(3*nW)+1];
        if(ImgIn[n+(3*nW)+2]<minB) minB=ImgIn[n+(3*nW)+2];      
      }

      ImgOut[n]=minR;
      ImgOut[n+1]=minG;
      ImgOut[n+2]=minB;
     }
   }

    for (int i=1; i < nH; i++){
     for (int j=0; j < 3*nW; j+=3){
      int n = (i*3*nW)+j;
      int maxR=ImgOut[n];
      int maxG=ImgOut[n+1];
      int maxB=ImgOut[n+2];

      if(j!=0){
        if(ImgOut[n-3]>maxR) maxR=ImgOut[n-3];
        if(ImgOut[n-2]>maxG) maxG=ImgOut[n-2];
        if(ImgOut[n-1]>maxB) maxB=ImgOut[n-1];
      }

      if(j!=(3*nW)-3){
        if(ImgOut[n+3]>maxR) maxR=ImgOut[n+3];
        if(ImgOut[n+4]>maxG) maxG=ImgOut[n+4];
        if(ImgOut[n+5]>maxB) maxB=ImgOut[n+5];
      }

      if(i!=0){
        if(ImgOut[n-(3*nW)]>maxR) maxR=ImgOut[n-(3*nW)];
        if(ImgOut[n-(3*nW)+1]>maxG) maxG=ImgOut[n-(3*nW)+1];
        if(ImgOut[n-(3*nW)+2]>maxB) maxB=ImgOut[n-(3*nW)+2];      
      }

      if(i!=nH-1){
        if(ImgOut[n+(3*nW)]>maxR) maxR=ImgOut[n+(3*nW)];
        if(ImgOut[n+(3*nW)+1]>maxG) maxG=ImgOut[n+(3*nW)+1];
        if(ImgOut[n+(3*nW)+2]>maxB) maxB=ImgOut[n+(3*nW)+2];      
      }

      ImgOut2[n]=maxR;
      ImgOut2[n+1]=maxG;
      ImgOut2[n+2]=maxB;
     }
   }

   ecrire_image_ppm(cNomImgEcrite, ImgOut2, nH, nW);
   free(ImgIn); free(ImgOut); free(ImgOut2);
   return 1;
}