// difference.cpp : dilate l'image puis garde la différence avec l'image d'origine pour obtenir les contours de l'objet

#include <stdio.h>
#include "image_ppm.h"



int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;

    
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut, *ImgOut2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgOut2, OCTET, nTaille);

	
   for (int i=0; i < nTaille; i++){
    ImgOut[i]=ImgIn[i];
   }


   
   for (int i=0; i < nH; i++){
     for (int j=0; j < nW; j++){
      int n = i*nW+j;
      if (ImgIn[n]==0)
      {
        if(j!=0) ImgOut[n-1]=0;
        if(j!=nW-1) ImgOut[n+1]=0;
        if(i!=0) ImgOut[n-nW]=0;
        if(i!=nH-1) ImgOut[n+nW]=0;
      }
     }
   }

   for (int i=0; i < nTaille; i++){
    if(ImgOut[i]==ImgIn[i]) ImgOut2[i]=255;
    else ImgOut2[i]=0;
   }   

   ecrire_image_pgm(cNomImgEcrite, ImgOut2,  nH, nW);
   free(ImgIn); free(ImgOut); free(ImgOut2);

   return 1;
}