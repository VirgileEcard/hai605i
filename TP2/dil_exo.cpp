// dilatation.cpp : augmente la taille de l'objet pour essayer de boucher les trous sur le fond

#include <stdio.h>
#include "image_ppm.h"

void dil(OCTET * ImgIn, OCTET * ImgOut, int nH, int nW){

  for (int i=0; i < nH*nW; i++){
    ImgOut[i]=ImgIn[i];
   }
          for (int i=0; i < nH; i++){
           for (int j=0; j < nW; j++){
            int n = i*nW+j;
            if (ImgIn[n]==0)
            {
              bool haut = (i==0);
              bool bas = (i==nH-1);
              bool droite = (j==nW-1);
              bool gauche = (j==0);
              if(!(haut&&droite)) ImgOut[(i-1)*nW+(j-1)]=0;
              if(!(haut&&gauche)) ImgOut[(i-1)*nW+(j+1)]=0;
              if(!(bas&&droite)) ImgOut[(i+1)*nW+(j-1)]=0;
              if(!(bas&&gauche)) ImgOut[(i+1)*nW+(j+1)]=0;
              if(!gauche) ImgOut[n-1]=0;
              if(!droite) ImgOut[n+1]=0;
              if(!haut) ImgOut[n-nW]=0;
              if(!bas) ImgOut[n+nW]=0;
            }
           }
         }
}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nbDil;

    
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Nb_dilatations\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&nbDil);

   OCTET *ImgIn, *ImgOut, *ImgOut2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgOut2, OCTET, nTaille);

	



   dil(ImgIn, ImgOut, nH, nW);

   if(nbDil%2){
    for(int k=1; k<nbDil;k+=2){
      dil(ImgOut, ImgOut2, nH, nW);
      dil(ImgOut2, ImgOut, nH, nW);
    }
    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   }
   else{
    dil(ImgOut, ImgOut2, nH, nW);
    for(int k=2; k<nbDil;k+=2){
      dil(ImgOut2, ImgOut, nH, nW);
      dil(ImgOut, ImgOut2, nH, nW);
    }
    ecrire_image_pgm(cNomImgEcrite, ImgOut2,  nH, nW);
   }
   for(int k=0; k<nbDil;k++){

   }

   free(ImgIn); free(ImgOut); free(ImgOut2);

   return 1;
}