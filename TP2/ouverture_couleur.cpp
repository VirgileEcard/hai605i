// ouverture_couleur.cpp : applique l'ouverture à une image .ppm

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);


   OCTET *ImgIn, *ImgOut, *ImgOut2;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);
   allocation_tableau(ImgOut2, OCTET, nTaille3);


   for (int i=1; i < nH; i++){
     for (int j=0; j < 3*nW; j+=3){
      int n = (i*3*nW)+j;
      int maxR=ImgIn[n];
      int maxG=ImgIn[n+1];
      int maxB=ImgIn[n+2];

      if(j!=0){
        if(ImgIn[n-3]>maxR) maxR=ImgIn[n-3];
        if(ImgIn[n-2]>maxG) maxG=ImgIn[n-2];
        if(ImgIn[n-1]>maxB) maxB=ImgIn[n-1];
      }

      if(j!=(3*nW)-3){
        if(ImgIn[n+3]>maxR) maxR=ImgIn[n+3];
        if(ImgIn[n+4]>maxG) maxG=ImgIn[n+4];
        if(ImgIn[n+5]>maxB) maxB=ImgIn[n+5];
      }

      if(i!=0){
        if(ImgIn[n-(3*nW)]>maxR) maxR=ImgIn[n-(3*nW)];
        if(ImgIn[n-(3*nW)+1]>maxG) maxG=ImgIn[n-(3*nW)+1];
        if(ImgIn[n-(3*nW)+2]>maxB) maxB=ImgIn[n-(3*nW)+2];      
      }

      if(i!=nH-1){
        if(ImgIn[n+(3*nW)]>maxR) maxR=ImgIn[n+(3*nW)];
        if(ImgIn[n+(3*nW)+1]>maxG) maxG=ImgIn[n+(3*nW)+1];
        if(ImgIn[n+(3*nW)+2]>maxB) maxB=ImgIn[n+(3*nW)+2];      
      }

      ImgOut[n]=maxR;
      ImgOut[n+1]=maxG;
      ImgOut[n+2]=maxB;
     }
   }

   for (int i=1; i < nH; i++){
     for (int j=0; j < 3*nW; j+=3){
      int n = (i*3*nW)+j;
      int minR=ImgOut[n];
      int minG=ImgOut[n+1];
      int minB=ImgOut[n+2];

      if(j!=0){
        if(ImgOut[n-3]<minR) minR=ImgOut[n-3];
        if(ImgOut[n-2]<minG) minG=ImgOut[n-2];
        if(ImgOut[n-1]<minB) minB=ImgOut[n-1];
      }

      if(j!=(3*nW)-3){
        if(ImgOut[n+3]<minR) minR=ImgOut[n+3];
        if(ImgOut[n+4]<minG) minG=ImgOut[n+4];
        if(ImgOut[n+5]<minB) minB=ImgOut[n+5];
      }

      if(i!=0){
        if(ImgOut[n-(3*nW)]<minR) minR=ImgOut[n-(3*nW)];
        if(ImgOut[n-(3*nW)+1]<minG) minG=ImgOut[n-(3*nW)+1];
        if(ImgOut[n-(3*nW)+2]<minB) minB=ImgOut[n-(3*nW)+2];      
      }

      if(i!=nH-1){
        if(ImgOut[n+(3*nW)]<minR) minR=ImgOut[n+(3*nW)];
        if(ImgOut[n+(3*nW)+1]<minG) minG=ImgOut[n+(3*nW)+1];
        if(ImgOut[n+(3*nW)+2]<minB) minB=ImgOut[n+(3*nW)+2];      
      }

      ImgOut2[n]=minR;
      ImgOut2[n+1]=minG;
      ImgOut2[n+2]=minB;
     }
   }

   ecrire_image_ppm(cNomImgEcrite, ImgOut, nH, nW);
   free(ImgIn); free(ImgOut);
   return 1;
}