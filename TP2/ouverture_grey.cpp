// ouverture_grey.cpp : applique ouverture à une image en niveaux de gris

#include <stdio.h>
#include "image_ppm.h"



int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;

    
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut, *ImgOut2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgOut2, OCTET, nTaille);


   
   for (int i=0; i < nH; i++){
     for (int j=0; j < nW; j++){
      int n = i*nW+j;
      int max=ImgIn[n];
      if(j!=0 && ImgIn[n-1]>max) max=ImgIn[n-1];
      if(j!=nW-1 && ImgIn[n+1]>max) max=ImgIn[n+1];
      if(i!=0 && ImgIn[n-nW]>max) max=ImgIn[n-nW];
      if(i!=nH-1 && ImgIn[n+nW]>max) max=ImgIn[n+nW];
      ImgOut[n]=max;
     }
   }

   for (int i=0; i < nH; i++){
     for (int j=0; j < nW; j++){
      int n = i*nW+j;
      int min=ImgOut[n];
      if(j!=0 && ImgOut[n-1]<min) min=ImgOut[n-1];
      if(j!=nW-1 && ImgOut[n+1]<min) min=ImgOut[n+1];
      if(i!=0 && ImgOut[n-nW]<min) min=ImgOut[n-nW];
      if(i!=nH-1 && ImgOut[n+nW]<min) min=ImgOut[n+nW];
      ImgOut2[n]=min;
     }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut2,  nH, nW);
   free(ImgIn); free(ImgOut); free(ImgOut2);

   return 1;
}
