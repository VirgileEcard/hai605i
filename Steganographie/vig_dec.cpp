// vig.cpp : decode une image chiffrée avec vig.cpp

#include <stdio.h>
#include "image_ppm.h"
#define LG_CLE 1669

int sousMod(int x, int y, int m){
  if (x>=y) return x-y;
  else return m+(x-y);
}

void lire_cle(char* nomFichier, int * cle){
  FILE *f;
  f =fopen(nomFichier, "r");
  char ch;
  int i = 0;

  if(f==NULL){
    printf("Erreur de lecture du fichier de la cle");
    exit(1);
  }

  while((ch=fgetc(f))!=EOF){
    if(ch!='\n'){
      cle[i++]=ch-'0';
    }
  }
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], cNomCle[250];
  int nH, nW, nTaille;
  int cle[LG_CLE];

  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm cle.txt \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%s",cNomCle) ;


   OCTET *ImgIn, *ImgTemp, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgTemp, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   lire_cle(cNomCle, cle);


   for (int i = 0; i < nTaille; ++i)
   {
    ImgTemp[i]=sousMod( ImgIn[i]/32, cle[i%LG_CLE], 8);  
   }

    for (int i=0; i < nTaille; i++){
      ImgOut[i]=ImgTemp[i]*32;
    }



	

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut); free(ImgTemp);

   return 1;
}