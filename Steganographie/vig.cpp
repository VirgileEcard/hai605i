// vig.cpp : chiffre une image en niveaux de gris sur 3 bits
// avec un chiffrement de Vigenere trivial. Utilisé seulement pour homogénéiser la distribution
// de l'histogramme de l'image produite par le programme de stéganographie
 

#include <stdio.h>
#include <stdlib.h>
#include "image_ppm.h"
#define LG_CLE 1669

void lire_cle(char* nomFichier, int * cle){
  FILE *f;
  f =fopen(nomFichier, "r");
  char ch;
  int i = 0;

  if(f==NULL){
    printf("Erreur de lecture du fichier de la cle");
    exit(1);
  }

  while((ch=fgetc(f))!=EOF){
    if(ch!='\n'){
      cle[i++]=ch-'0';
    }
  }
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250], cNomCle[250];
  int nH, nW, nTaille;
  int Vig[8][8];
  int cle[LG_CLE];


  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm cle.txt\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%s",cNomCle) ;


   OCTET *ImgIn, *ImgTemp, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   allocation_tableau(ImgTemp, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);



    for (int i = 0; i < 8; i++)
    {
      for (int j = 0; j < 8; j++)
      {
        Vig[i][j]=(i+j) %8;
      }
    }

    lire_cle(cNomCle, cle);


    for (int i=0; i < nTaille; i++){
    OCTET k=0;
      for (int s=32; s<=256; s+=32){
        if ((ImgIn[i]<s)&&(ImgIn[i]>=s-32)){
          ImgTemp[i] = k;
        }

        k++;
      }
    }

   for (int i = 0; i < nTaille; ++i)
   {
    ImgOut[i]=Vig[ImgTemp[i]][cle[i%LG_CLE]]*32;  
   }

	

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut); free(ImgTemp);

   return 1;
}
