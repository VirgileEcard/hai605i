// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int Niv[2]; //On fait un tableau pour garder le nombre de pixels noirs ou blancs
  int Essaye[256]; //On garde les seuils déjà essayés pour eviter de se retrouver dans une boucle infinie
  int nH, nW, nTaille, S=128;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for (int i=0; i < 256; i++)
    {
     Essaye[i]=0;
   }


  do{ 
    Niv[0]=Niv[1]=0;
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
        if ( ImgIn[i*nW+j] < S){
          ImgOut[i*nW+j]=0;
          Niv[0]++;
        }
        else {
          ImgOut[i*nW+j]=255;
          Niv[1]++;
        }
       }
       Essaye[S]=1;
       Niv[0]<Niv[1]?S++:S--;
     }
     while(((Niv[0]>Niv[1])|(Niv[0]<Niv[1]))&&(!Essaye[S]));





   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
