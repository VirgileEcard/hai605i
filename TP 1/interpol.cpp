/* interpol.cpp : Seuille une image en niveau de gris de la manière suivante :
 *Un pixel <S1 est seuillé à 0
 *Un pixel >S2 est seuillé à 255
 *Un pixel entre les deux est interpolé avec une fonction au choix de l'utilisateur*/

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

//On définit avant le main les fonction utilisables pour l'interpolation
int sinus(float t){
return 255*(sin(t*1.57));
}

int cube(float t){
return (t*t*t)*255;
}

int Marie_Josephine(float t){
return (3*(t*t)-2*(t*t*t))*255;
}

int def(float t){
  return t*255;
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, S1, S2;
  int (*fct) (float);


  if (argc != 6) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil1 Seuil2 fct, avec Seuils classés et fct dans {sin, cube, zarb}\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&S1);
   sscanf (argv[4],"%d",&S2);

   //On assigne la fonction à utiliser à fct suivant l'argument passé
   if (strcmp(argv[5],"sin")==0) fct = sinus;
   else if (strcmp(argv[5],"cube")==0) fct = cube;
   else if (strcmp(argv[5],"zarb")==0) fct = Marie_Josephine;
   else fct = def;


   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
         float t=((float)(ImgIn[i*nW+j]-S1)/(float)(S2-S1)); //On calcule le t à utiliser dans les fonctions

         if ( ImgIn[i*nW+j] < S1) ImgOut[i*nW+j]=0;
         else if ( ImgIn[i*nW+j] < S2) ImgOut[i*nW+j]=fct(t); //Le pixel est compris entre les seuils,
                                                              //on lui applique la fonction d'interpolation
         else ImgOut[i*nW+j]=255;
       }

  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn); free(ImgOut);

  return 1;
}