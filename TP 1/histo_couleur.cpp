/*histo_couleur.cpp produit un histogramme en .dat des niveaux de couleurs d'une image ppm
 *dans le fichier produit, on aura 4 colonnes :
 *-le niveau de couleurs
 *-le nombre de pixels dont la composante rouge correspond à ce niveau
 *-                                       verte
 *-                                       bleue                         */

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char const *argv[])
{
	char cNomImgLue[250], cNomHisto[250];
	int Niv[256][3];
	int nHeight, nWidth, nTaille;

	if (argc != 3) 
     {
       printf("Usage: Image.pgm HistoOut.dat \n"); 
       exit (1) ;
     }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomHisto);
    

    OCTET *ImgIn; //on déf le tableau dans lequel on va lire l'image

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nHeight, &nWidth);
    nTaille = nHeight * nWidth;

    int nTaille3 = 3*nTaille;

    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nHeight * nWidth);

    for (int i = 0; i<256; i++)
    {
    	Niv[i][0]=0;
      Niv[i][1]=0;
      Niv[i][2]=0;

    }


    //On somme chaque pixel dans les 3 cases de Niv correspondant à l'intensité de chacune de leur composantes couleur
    for (int i = 0; i<nTaille3; i+=3)
    {
    	Niv[ImgIn[i]][0]++;
      Niv[ImgIn[i+1]][1]++;
      Niv[ImgIn[i+2]][2]++;
    }

    
   FILE *f_plot;

   if( (f_plot = fopen(cNomHisto, "wb")) == NULL)
      {
	 printf("\nPas d'acces en ecriture sur le fichier %s \n", cNomHisto);
	 exit(EXIT_FAILURE);
      }


    for (int i = 0; i<256; i++)
    {
    	fprintf(f_plot, "%d  %d  %d  %d\n", i, Niv[i][0], Niv[i][1], Niv[i][2] );
    }


	 fclose(f_plot);




	return 0;
}