// seuil_auto.cpp : seuille une image en noir et blanc en utilisant la méthode d'Otsu

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int Niv[256]; //Pour stocker l'histogramme
  int nH, nW, nTaille, S;
  int q1=0;
  int q2=0;
  float mu1=0.0;
  float mu2=0.0;
  float var=0.0;
  float varmax=0.0;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
    //Création de l'histogramme
    for (int i = 0; i<256; i++)
    {
      Niv[i]=0;
    }

    for (int i = 0; i<nTaille; i++)
    {
      Niv[ImgIn[i]]++;
    }


    //Somme du produit de tous les pixels par leur intensité, utilisée pour simplifier le calcul de q2
    int som=0;
    for(int i=0; i<256; i++){
      som+=(Niv[i]*i);
    }

    int somB=0;

    //On calcule les moyennes d'intensité des classes (ens des pixels <= ou > au seuil) pour chaque seuil
    for(int t=0; t<256; t++){
      //q1 # de pixels en-dessous du seuil, q2 au-dessus
      q1+=Niv[t];
      if(!q1) continue;

      q2=nTaille-q1;

      somB+=Niv[t]*t; //Somme du produit pixels en-dessous du seuil par leur intensité
      mu1 = (float)somB / (float)q1;
      mu2 = (float)(som - somB) / (float)q2;

      var = (float)(q1*q1) * pow((mu1-mu2), 2); //variance interclasse avec le seuil t

      if (var > varmax){  //On cherche à maximiser cette variance
        S=t;
        varmax=var;
      }
    }

   for (int i=0; i < nH; i++)
     for (int j=0; j < nW; j++)
       {
         if ( ImgIn[i*nW+j] < S) ImgOut[i*nW+j]=0; else ImgOut[i*nW+j]=255;
       }


   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}