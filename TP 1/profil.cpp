//profil.cpp produit un histogramme en .dat du profil d'une ligne ou colonne de l'image

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char const *argv[])
{
	char cNomImgLue[250], cNomHisto[250];
  	int nHeight, nWidth, nTaille;
  char cLouC;
  int coord;

	if (argc != 5) 
     {
       printf("Usage: Image.pgm HistoOut.dat i X \nAvec i la coordonnee de la ligne ou colonne etudiee et X remplace par C ou L pour choisir ligne ou colonne"); 
       exit (1) ;
     }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomHisto);
    sscanf (argv[3],"%d",&coord);
    sscanf (argv[4],"%c",&cLouC);

    

    OCTET *ImgIn;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nHeight, &nWidth);
    nTaille = nHeight * nWidth;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nHeight * nWidth);

    //On définit le tableau dans lequel on va mettre les niveaux de gris de la colonne ou de la ligne
    //Sa longueur sera respectivement la hauteur ou la largeur
    int lgTab = (cLouC=='C')? nHeight : nWidth;
    int Prof[lgTab];

    for(int i = 0; i<lgTab; i++){
        Prof[i]= ImgIn[(cLouC=='C')?(i*nWidth+coord):(coord*nWidth+i)];
    }


    //On écrit Prof dans un fichier .dat pour pouvoir l'utiliser avec GNUplot
   FILE *f_plot;

   if( (f_plot = fopen(cNomHisto, "wb")) == NULL)
      {
	 printf("\nPas d'acces en ecriture sur le fichier %s \n", cNomHisto);
	 exit(EXIT_FAILURE);
      }


    for (int i = 0; i<lgTab; i++)
    {
    	fprintf(f_plot, "%d  %d\n", i, Prof[i] );
    }


	 fclose(f_plot);




	return 0;
}