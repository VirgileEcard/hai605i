// test_grey_choix.cpp : Seuille une image en niveau de gris suivant un nombre quelconque de seuils donnés par l'utilisateur
// l'intensité d'un pixel se trouvant entre deux seuils sera changé en la moyenne de ces derniers

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  int nbS = argc-3;
  int Seuils[nbS+1]; //stockage des seuils
  Seuils[0]=0;
  
  if (argc < 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuils, avec seuils classés\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);


   for(int i = 1; i < nbS+1; i++){
    sscanf(argv[i+2],"%d", Seuils+(i));
   }

   int Moy[nbS+1]; //stockage des moyennes entre tous les seuils
   Moy[0]=0;
   Moy[nbS]=255;

   for(int i = 1; i < nbS; i++){
    Moy[i]=(Seuils[i]+Seuils[i+1])/2;
   }


   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
    for (int k=0; k<nbS+1; k++){
      for (int i=0; i < nH; i++){
        for (int j=0; j < nW; j++){
          if ( ImgIn[i*nW+j] < Seuils[k+1] && ImgIn[i*nW+j]>= Seuils[k]) ImgOut[i*nW+j]=Moy[k];
        }
      }
    }


   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
