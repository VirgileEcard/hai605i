// seuil_auto_naif_multi.cpp : Seuille une image en 4 niveau de gris en cherchant que le # de pixels de chaque niveau soit le même

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  int Seuils[3]={0, 0, 0};
  int Essaye[256];
  int Niv = 0;
  int moy1, moy2;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }

   for (int i=0; i < 256; i++)
    {
     Essaye[i]=0;
   }


    while((Niv!=(nTaille/4 )) && (!Essaye[Seuils[0]]))
    {
      Niv=0;
      printf("On tente avec un seuil 1 de %d\n", Seuils[0]);
      for (int i=0; i < nH; i++)
         for (int j=0; j < nW; j++)
           {
             if ( ImgIn[i*nW+j] < Seuils[0]){
              ImgOut[i*nW+j]=0;
              Niv++;
            }
           }

        Essaye[Seuils[0]]=1;
        (Niv<nTaille/4)?Seuils[0]++:Seuils[0]--;
        printf("Le Niv est de %d\n", Niv);
    }


  Seuils[1]=Seuils[0]+2;
  Niv=0;

  while((Niv!=(nTaille/4 )) && (!Essaye[Seuils[1]]))
  {
    Niv=0;
    moy1 = (Seuils[0]+Seuils[1])/2;
    printf("On tente avec un seuil 2 de %d\n", Seuils[1]);
    for (int i=0; i < nH; i++)
       for (int j=0; j < nW; j++)
         {
           if ((ImgIn[i*nW+j] < Seuils[1])&&(ImgIn[i*nW+j] >= Seuils[0])){
            ImgOut[i*nW+j]=moy1;
            Niv++;
          }
         }

      Essaye[Seuils[1]]=1;
      (Niv<nTaille/4)?Seuils[1]++:Seuils[1]--;
      printf("Le Niv est de %d\n", Niv);
  }
 

  Seuils[2]=Seuils[1]+2;
  Niv=0;

  while((Niv!=(nTaille/4 )) && (!Essaye[Seuils[2]]))
  {
    Niv=0;
    moy2 = (Seuils[1]+Seuils[2])/2;
    printf("On tente avec un seuil 3 de %d\n", Seuils[2]);
    for (int i=0; i < nH; i++)
       for (int j=0; j < nW; j++)
         {
           if ((ImgIn[i*nW+j] < Seuils[2])&&(ImgIn[i*nW+j] >= Seuils[1])){
            ImgOut[i*nW+j]=moy2;
            Niv++;
          }
          else if (ImgIn[i*nW+j] >= Seuils[2]) ImgOut[i*nW+j]=255;
         }

      Essaye[Seuils[2]]=1;
      (Niv<nTaille/4)?Seuils[2]++:Seuils[2]--;
      printf("Le Niv est de %d\n", Niv);
  }


   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}