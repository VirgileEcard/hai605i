// quant.cpp : sous-échantillonne une image selon le nombre de valeurs passées en paramètre

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nbVal;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm nbVal\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s", cNomImgEcrite);
   sscanf (argv[3],"%d", &nbVal);


   OCTET *ImgIn, *ImgOut;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int n=0; n<nTaille; n++){
    ImgOut[n]=(OCTET)(nbVal*((float)ImgIn[n]/255.)) * (255./(float)nbVal);
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}