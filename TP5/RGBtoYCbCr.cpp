/*RGBtoYCbCR.cpp : transorme une image ppm en 3 images pgm de ses composantes Y, Cb et Cr*/

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char const *argv[])
{
	char cNomImgLue[250], cNomImgY[250], cNomImgCb[250], cNomImgCr[250];
	int nHeight, nWidth, nTaille;

	if (argc != 5) 
     {
       printf("Usage: ImageIn.ppm Y.pgm Cb.pgm Cr.pgm\n"); 
       exit (1) ;
     }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgY);
    sscanf (argv[3],"%s",cNomImgCb);
    sscanf (argv[4],"%s",cNomImgCr);

    

    OCTET *ImgIn, *ImgY, *ImgCb, *ImgCr;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nHeight, &nWidth);
    nTaille = nHeight * nWidth;

    int nTaille3 = 3*nTaille;

    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nHeight * nWidth);
    allocation_tableau(ImgY, OCTET, nTaille);
    allocation_tableau(ImgCb, OCTET, nTaille);
    allocation_tableau(ImgCr, OCTET, nTaille);


    for(int n=0; n<nTaille; n++){
      ImgY[n]=(OCTET)( (0.299*ImgIn[n*3]) + (0.587*ImgIn[n*3 + 1]) + (0.114*ImgIn[n*3 + 2]) );
      ImgCb[n]=(OCTET)( (-0.1687*ImgIn[n*3]) + (-0.3313*ImgIn[n*3 + 1]) + (0.5*ImgIn[n*3 + 2]) + 128.);
      ImgCr[n]=(OCTET)( (0.5*ImgIn[n*3]) + (-0.4187*ImgIn[n*3 + 1]) + (-0.0813*ImgIn[n*3 + 2]) + 128.);

    }

    ecrire_image_pgm(cNomImgY, ImgY,  nHeight, nWidth);
    ecrire_image_pgm(cNomImgCb, ImgCb,  nHeight, nWidth);
    ecrire_image_pgm(cNomImgCr, ImgCr,  nHeight, nWidth);

    free(ImgIn); free(ImgY); free(ImgCb); free(ImgCr);


	return 0;
}