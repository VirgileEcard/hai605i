/*RGBtoY : à partir d'une image ppm, produit une image pgm de sa composante Y*/

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char const *argv[])
{
	char cNomImgLue[250], cNomImgEcrite[250];
	int nHeight, nWidth, nTaille;

	if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.pgm \n"); 
       exit (1) ;
     }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nHeight, &nWidth);
    nTaille = nHeight * nWidth;

    int nTaille3 = 3*nTaille;

    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nHeight * nWidth);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for(int n=0; n<nTaille; n++){
      ImgOut[n]=(OCTET)( (0.299*ImgIn[n*3]) + (0.587*ImgIn[n*3 + 1]) + (0.114*ImgIn[n*3 + 2]) );
    }

    ecrire_image_pgm(cNomImgEcrite, ImgOut,  nHeight, nWidth);
    free(ImgIn); free(ImgOut);


	return 0;
}