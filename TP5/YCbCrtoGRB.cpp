/*YCbCrtoGRB.cpp : produit une image ppm en GRB à partir de 3 images pgm représentant des composantes Y, Cb et Cr*/

#include <stdio.h>
#include "image_ppm.h"
#define MAX(X, Y) (((X) < (Y)) ? (Y) : (X))


int main(int argc, char const *argv[])
{
	char cNomImgEcrite[250], cNomImgY[250], cNomImgCb[250], cNomImgCr[250];
	int nH, nW, nTaille;

	if (argc != 5) 
     {
       printf("Usage: Y.pgm Cb.pgm Cr.pgm ImageOut.ppm\n"); 
       exit (1) ;
     }

    sscanf (argv[4],"%s",cNomImgEcrite) ;
    sscanf (argv[1],"%s",cNomImgY);
    sscanf (argv[2],"%s",cNomImgCb);
    sscanf (argv[3],"%s",cNomImgCr);

    

    OCTET *ImgOut, *ImgY, *ImgCb, *ImgCr;

    lire_nb_lignes_colonnes_image_pgm(cNomImgY, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = 3*nTaille;

    allocation_tableau(ImgOut, OCTET, nTaille3);
    allocation_tableau(ImgY, OCTET, nTaille);
    lire_image_pgm(cNomImgY, ImgY, nH * nW);
    allocation_tableau(ImgCb, OCTET, nTaille);
    lire_image_pgm(cNomImgCb, ImgCb, nH * nW);
    allocation_tableau(ImgCr, OCTET, nTaille);
    lire_image_pgm(cNomImgCr, ImgCr, nH * nW);


    for(int n=0; n<nTaille; n++){
      ImgOut[n*3 + 1] = (OCTET) MAX(0, (ImgY[n] + (1.402 * (ImgCr[n] - 128.))));
      ImgOut[n*3 + 0] = (OCTET) MAX(0, (ImgY[n] - (0.34414 * (ImgCb[n] - 128.)) - (0.714414*(ImgCr[n] - 128.))  ));
      ImgOut[n*3 + 2] = (OCTET) MAX(0, (ImgY[n] + (1.772 * (ImgCb[n] - 128.))));
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);

    free(ImgOut); free(ImgY); free(ImgCb); free(ImgCr);


	return 0;
}