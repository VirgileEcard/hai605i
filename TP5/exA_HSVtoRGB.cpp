/*HSVtoRGB.cpp : construit une image ppm à partir de trois images pgm représentant
ses composantes HSV*/

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"
#define MAX(X, Y) (((X) < (Y)) ? (Y) : (X))
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))



int main(int argc, char const *argv[])
{
	char cNomImgEcrite[250], cNomImgH[250], cNomImgS[250], cNomImgV[250];
	int nH, nW, nTaille;
  float C, m, X, H, S, V, R, G, B;

	if (argc != 5) 
     {
       printf("Usage: H.pgm S.pgm V.pgm ImageOut.ppm\n"); 
       exit (1) ;
     }

    sscanf (argv[4],"%s",cNomImgEcrite) ;
    sscanf (argv[1],"%s",cNomImgH);
    sscanf (argv[2],"%s",cNomImgS);
    sscanf (argv[3],"%s",cNomImgV);

    

    OCTET *ImgOut, *ImgH, *ImgS, *ImgV;

    lire_nb_lignes_colonnes_image_pgm(cNomImgH, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = 3*nTaille;

    allocation_tableau(ImgOut, OCTET, nTaille3);
    allocation_tableau(ImgH, OCTET, nTaille);
    lire_image_pgm(cNomImgH, ImgH, nH * nW);
    allocation_tableau(ImgS, OCTET, nTaille);
    lire_image_pgm(cNomImgS, ImgS, nH * nW);
    allocation_tableau(ImgV, OCTET, nTaille);
    lire_image_pgm(cNomImgV, ImgV, nH * nW);


    for(int n=0; n<nTaille; n++){
      /******Calcul de C et X******/
      H = (float)(ImgH[n]*(360./255.));
      S = (float)(ImgS[n]/255.);
      V = (float)(ImgV[n]/255.);
      C = (V*S);

      /********On détermine quelle permutation de (C, X, 0) affecter à RGB*********/
      m = V-C;
      float Hdec = (H/60.) - floor(H/60.);
      X = C * (1. - abs(   ((int)(H/60)%2) + Hdec - 1)  );


      if(H<60){
        R = C + m;
        G = X + m;
        B = m;
      }
      else if(H<120){
        R = X + m;
        G = C + m;
        B = m;
      }
      else if(H<180){
        R = m;
        G = C + m;
        B = X + m;
      }
      else if(H<240){
        R = m;
        G = X + m;
        B = C + m;
      }
      else if(H<300){
        R = X + m;
        G = m;
        B = C + m;
      }
      else{
        R = C + m;
        G = m;
        B = X + m;
      }



      ImgOut[n*3    ] = (int)(R*255);
      ImgOut[n*3 + 1] = (int)(G*255);
      ImgOut[n*3 + 2] = (int)(B*255);
    }

    ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);

    free(ImgOut); free(ImgH); free(ImgS); free(ImgV);


	return 0;
}