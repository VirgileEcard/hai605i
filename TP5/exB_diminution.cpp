// diminution.cpp :  divise la taille de l'image par quatre

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s", cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille/4);
	
   for(int x=0; x<(nW/2); x++){
    for (int y = 0; y<(nH/2); y++)
    {
      ImgOut[x*(nW/2)+y]=ImgIn[(2*x)*nW+(2*y)];
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH/2, nW/2);
   free(ImgIn); free(ImgOut);

   return 1;
}