// agrandissement.cpp : multiplie la taille de l'image par quatre en diffusant simplement un pixel a trois voisins

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s", cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille*4);
	
   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[(x*2)*(nW*2)+(y*2)]=ImgOut[((x*2)+1)*(nW*2)+(y*2)]=ImgOut[(x*2)*(nW*2)+((y*2)+1)]=ImgOut[((x*2)+1)*(nW*2)+((y*2)+1)]=ImgIn[x*nW+y];
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH*2, nW*2);
   free(ImgIn); free(ImgOut);

   return 1;
}