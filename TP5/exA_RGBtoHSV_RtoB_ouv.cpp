/*RGBtoHSV : à partir d'une image RGB, produit des images de ses composantes HSV*/


#include <stdio.h>
#include <math.h>
#include "image_ppm.h"
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) < (Y)) ? (Y) : (X))

int main(int argc, char const *argv[])
{
	char cNomImgLue[250], cNomImgH[250], cNomImgS[250], cNomImgV[250];
	int nH, nW, nTaille, H, S, V, Xmin, Chr, R, G, B, delta, Hrech;

	if (argc != 7) 
     {
       printf("Usage: ImageIn.ppm H.pgm S.pgm V.pgm H delta\n"); 
       exit (1) ;
     }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgH);
    sscanf (argv[3],"%s",cNomImgS);
    sscanf (argv[4],"%s",cNomImgV);
    sscanf (argv[5],"%d",&Hrech);
    sscanf (argv[6],"%d",&delta);


    

    OCTET *ImgIn, *ImgH, *ImgS, *ImgV, *ImgTemp, *ImgTemp2;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = 3*nTaille;

    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgH, OCTET, nTaille);
    allocation_tableau(ImgS, OCTET, nTaille);
    allocation_tableau(ImgV, OCTET, nTaille);
    allocation_tableau(ImgTemp, OCTET, nTaille);
    allocation_tableau(ImgTemp2, OCTET, nTaille);




    for(int n=0; n<nTaille; n++){
      R = ImgIn[n*3];
      G = ImgIn[n*3+1];
      B = ImgIn[n*3+2];

      /******Calcul de V, composant min et Chr******/
      V = MAX(R, MAX(G, B));
      Xmin = MIN(R, MIN(G, B));
      Chr = V-Xmin;

      /*****Calcul de H*********/
      if(!Chr) H=0;
      else if(V==R) H = (int)((((float)(G-B)/(float)Chr) * (255./6.)) + 255) %255;
      else if(V==G) H = (int)((((float)(B-R)/(float)Chr) * (255./6.)) + 85) %255;
      else H = (int)((((float)(R-G)/(float)Chr) * (255./6.)) + 170) %255;

      H = (H+255)%255;
      //printf("%d\n", H);

      /******Calcul de S******/
      if(!V) S = 0;
      else S = (OCTET)  (((float)Chr/(float)V) * 255);

      int dist = MIN(abs(Hrech - H), 255- abs(Hrech - H));

      if(dist<delta) ImgTemp[n]=255;
      else ImgTemp[n]=0;


      ImgH[n] = H;
      ImgS[n] = S;
      ImgV[n] = V;

    }

    /***On ouvre ImgTemp***/
    for(int i = 0; i<nH; i++){
      for(int j = 0; j<nH; j++){
        int n = i*nW+j;
        if (ImgIn[n]==0)
        {
          if(j!=0) ImgTemp[n-1]=0;
          if(j!=nW-1) ImgTemp[n+1]=0;
          if(i!=0) ImgTemp[n-nW]=0;
          if(i!=nH-1) ImgTemp[n+nW]=0;
        }
      }
    }
    for(int n=0; n<nTaille; n++){
      ImgTemp2[n]=ImgTemp[n];
    }
    for(int i = 0; i<nH; i++){
      for(int j = 0; j<nH; j++){
        int n = i*nW+j;
        if (ImgTemp[n]==255)
        {
          if(j!=0) ImgTemp2[n-1]=255;
          if(j!=nW-1) ImgTemp2[n+1]=255;
          if(i!=0) ImgTemp2[n-nW]=255;
          if(i!=nH-1) ImgTemp2[n+nW]=255;
        }
      }
    }

    /****On floute ImgTemp2****/

    

    for(int n=0; n<nTaille; n++){
      ImgH[n]=(ImgH[n]-(int)(85*(ImgTemp2[n]/255.)))%255;
    }

    ecrire_image_pgm(cNomImgH, ImgH,  nH, nW);
    ecrire_image_pgm(cNomImgS, ImgS,  nH, nW);
    ecrire_image_pgm(cNomImgV, ImgV,  nH, nW);

    free(ImgIn); free(ImgH); free(ImgS); free(ImgV);


	return 0;
}