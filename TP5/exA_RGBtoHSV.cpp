/*RGBtoHSV : à partir d'une image RGB, produit des images de ses composantes HSV*/

#include <stdio.h>
#include "image_ppm.h"
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) < (Y)) ? (Y) : (X))

int main(int argc, char const *argv[])
{
	char cNomImgLue[250], cNomImgH[250], cNomImgS[250], cNomImgV[250];
	int nH, nW, nTaille, H, S, V, Xmin, Chr, R, G, B;

	if (argc != 5) 
     {
       printf("Usage: ImageIn.ppm H.pgm S.pgm V.pgm\n"); 
       exit (1) ;
     }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgH);
    sscanf (argv[3],"%s",cNomImgS);
    sscanf (argv[4],"%s",cNomImgV);

    

    OCTET *ImgIn, *ImgH, *ImgS, *ImgV;

    lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    int nTaille3 = 3*nTaille;

    allocation_tableau(ImgIn, OCTET, nTaille3);
    lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgH, OCTET, nTaille);
    allocation_tableau(ImgS, OCTET, nTaille);
    allocation_tableau(ImgV, OCTET, nTaille);


    for(int n=0; n<nTaille; n++){
      R = ImgIn[n*3];
      G = ImgIn[n*3+1];
      B = ImgIn[n*3+2];

      /******Calcul de V, composant min et Chr******/
      V = MAX(R, MAX(G, B));
      Xmin = MIN(R, MIN(G, B));
      Chr = V-Xmin;

      /*****Calcul de H*********/
      if(!Chr) H=0;
      else if(V==R) H = (int)((((float)(G-B)/(float)Chr) * (255./6.)) + 255) %255;
      else if(V==G) H = (int)((((float)(B-R)/(float)Chr) * (255./6.)) + 85) %255;
      else H = (int)((((float)(R-G)/(float)Chr) * (255./6.)) + 170) %255;

      H = (H+255)%255;
      //printf("%d\n", H);

      /******Calcul de S******/
      if(!V) S = 0;
      else S = (OCTET)  (((float)Chr/(float)V) * 255);


      ImgH[n] = H;
      ImgS[n] = S;
      ImgV[n] = V;

    }

    ecrire_image_pgm(cNomImgH, ImgH,  nH, nW);
    ecrire_image_pgm(cNomImgS, ImgS,  nH, nW);
    ecrire_image_pgm(cNomImgV, ImgV,  nH, nW);

    free(ImgIn); free(ImgH); free(ImgS); free(ImgV);


	return 0;
}