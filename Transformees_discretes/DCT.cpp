// DCT.cpp : calcule la DCT de blocs 8x8 d'une image et produit une carte des DCT 
#include <stdio.h>
#include <math.h>
#include "image_ppm.h"
#define PI 3.141592



float dct(OCTET * Img, OCTET * ImgOut, int nW, int nH, int x, int y){
  int Pix[8][8];
  float G[8][8];

  int Simpl[8][8] = { {16, 11, 10, 16, 24, 40, 51, 61},
                      {12, 12, 14, 19, 26, 58, 60, 55},
                      {14, 13, 16, 24, 40, 57, 69, 56},
                      {14, 17, 22, 29, 51, 87, 80, 62},
                      {18, 22, 37, 56, 68,109,103, 77},
                      {24, 35, 55, 64, 81,104,113, 92},
                      {49, 64, 78, 87,103,121,120,101},
                      {72, 92, 95, 98,112,100,103, 99}
                    };

  /******Passage des pixels à un intervalle centré en zéro*********/
  for(int dx = 0; dx<8; dx++){
    for(int dy = 0; dy<8; dy++){
      Pix[dx][dy]= Img[(x+dx)*nW+(y+dy)] - 128;
    }
  }

  /*for(int i = 0; i<8; i++){
    for(int j= 0; j<8; j++){
      printf("%d ", Pix[i][j]);
    }
    printf("\n");
  }*/


/************calcul de la DCT du bloc**********/
  for(int u = 0; u<8; u++){
    for(int v = 0; v<8; v++){
      float alphaU = 1.;
      float alphaV = 1.;
      if (u==0) alphaU /= sqrt(2);
      if (v==0) alphaV /= sqrt(2);

      //printf("******DCT %d %d******\n", u, v);
      float som = 0.;
      for(int dx = 0; dx<8; dx++){
        for(int dy = 0; dy<8; dy++){
          //printf("Pixel %d %d\n", dx, dy);
          //printf(" premier cos : %.4f \n", cos(  ((2*dx + 1) * PI * u)/16));
          //printf(" second cos  : %.4f \n", cos(  ((2*dy + 1) * PI * v)/16));
          float dct1 = (float)Pix[dx][dy] * (cos( ((2*dx + 1) * PI * u) /16)) * (cos( ((2*dy + 1) * PI * v)/16));
          //printf("%.2f \n", dct1);
          som+=dct1;
        }
      }

      G[u][v] = 0.25 * alphaU * alphaV * som;
      ImgOut[(x+u)*nW+(y+v)] = (int)(G[u][v]/Simpl[u][v]) + 128;
      //printf("%d ", (ImgOut[(x+u)*nW+(y+v)] - 128) * Simpl[u][v]);


    }
        //printf("\n");

  }
  /*for(int i = 0; i<8; i++){
    for(int j= 0; j<8; j++){
      printf("%.2f ", G[i][j]);
    }
    printf("\n");
  }*/


  return G[0][0]; // permettra de connaitre la fourchette des coef pour savoir comment remplir la carte.
} 




int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgCarte[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm Map.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgCarte);



   OCTET *ImgIn, *ImgMap;

   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgMap, OCTET, nTaille);

	
   float max=0.;
   float min=0.;

   for(int x=0; x+8<=nH; x+=8){
    for (int y = 0; y+8<=nW; y+=8)
    {
      float m = dct(ImgIn, ImgMap, nH, nW, x, y);
      if (m<min) min = m;
      if (m>max) max = m;
    }
   }
   //dct(ImgIn, ImgMap, nH, nW, 0, 0);
   //printf("dct max = %.4f\ndct min = %.4f\n", max, min);

   ecrire_image_pgm(cNomImgCarte, ImgMap,  nH, nW);


   free(ImgIn); free(ImgMap);

   return 1;
}