// DFT.cpp : calcule la DFT d'une image pgm
#include <stdio.h>
#include <math.h>
#include "image_ppm.h"
#define PI 3.141592
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))




void dft(OCTET * Img, OCTET * ImgOut, int nW, int nH, int u, int v){
  float parR = 0.;
  float parI = 0.;
  float c = sqrt(nW*nH);

  for(int x = 0; x<nH; x++){
    //printf("ligne %d \n", x);
    for(int y = 0; y<nW; y++){
      parR += (Img[x*nW+y] * cos(2 * PI *( (((float)u*(float)x)/(float)nW)  + (((float)v*(float)y)/(float)nH) )  )) / c;
      parI -= (Img[x*nW+y] * sin(2 * PI *( (((float)u*(float)x)/(float)nW)  + (((float)v*(float)y)/(float)nH) )  )) / c;
    }
  }

  float arg = sqrt(pow(parR, 2) + pow(parI, 2));


  ImgOut[u*nW+v]=MIN(255, 18 * log2((int)arg));
  //printf("%d\n", ImgOut[u*nW+v]);
  //return arg;
} 




int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgCarte[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm Map.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgCarte);



   OCTET *ImgIn, *ImgMap;

   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgMap, OCTET, nTaille);

	
   //float max=0.;

   for(int x=0; x<nH; x++){
    printf("ligne %d\n", x);
    for (int y = 0; y<nW; y++)
    {
      //printf("dft en %d %d \n", x, y);
      dft(ImgIn, ImgMap, nH, nW, x, y);
      //if (m>max) max = m;
    }
   }
   //dct(ImgIn, ImgMap, nH, nW, 0, 0);
   //printf("dft max = %.4f\n", max);

   ecrire_image_pgm(cNomImgCarte, ImgMap,  nH, nW);


   free(ImgIn); free(ImgMap);

   return 1;
}