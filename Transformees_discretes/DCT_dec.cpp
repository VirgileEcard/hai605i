// DCT_dec.cpp : reconstruit une image pgm à partir d'une carte des DCT
#include <stdio.h>
#include <math.h>
#include "image_ppm.h"
#define PI 3.141592
#define MAX(X, Y) (((X) < (Y)) ? (Y) : (X))
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))




void dec(OCTET * Img, OCTET * ImgOut, int nW, int nH, int x, int y){
  float Pix[8][8];
  int G[8][8];

  int Simpl[8][8] = { {16, 11, 10, 16, 24, 40, 51, 61},
                      {12, 12, 14, 19, 26, 58, 60, 55},
                      {14, 13, 16, 24, 40, 57, 69, 56},
                      {14, 17, 22, 29, 51, 87, 80, 62},
                      {18, 22, 37, 56, 68,109,103, 77},
                      {24, 35, 55, 64, 81,104,113, 92},
                      {49, 64, 78, 87,103,121,120,101},
                      {72, 92, 95, 98,112,100,103, 99}
                    };

  /******Recréation d'une matrice de DCT à partir de l'image*********/
  for(int dx = 0; dx<8; dx++){
    for(int dy = 0; dy<8; dy++){
      G[dx][dy]= (Img[(x+dx)*nW+(y+dy)] - 128) * Simpl[dx][dy];
    }
  }

  /*for(int i = 0; i<8; i++){
    for(int j= 0; j<8; j++){
      printf("%d ", G[i][j]);
    }
    printf("\n");
  }*/


/************Reconstruction du bloc de pixels à partir de la matrice**********/
  for(int dx = 0; dx<8; dx++){
    for(int dy = 0; dy<8; dy++){

      //printf("******DCT %d %d******\n", u, v);
      float som = 0.;
      for(int u = 0; u<8; u++){
        for(int v = 0; v<8; v++){
          float alphaU = 1.;
          float alphaV = 1.;
          if (u==0) alphaU /= sqrt(2);
          if (v==0) alphaV /= sqrt(2);
          float pix1 = alphaU * alphaV * (float)G[u][v] * (cos( ((2*dx + 1) * PI * u) /16)) * (cos( ((2*dy + 1) * PI * v)/16));
          som+=pix1;
        }
      }

      Pix[dx][dy] = 0.25 * som;
      ImgOut[(x+dx)*nW+(y+dy)] = MIN(255, MAX(0 , Pix[dx][dy] + 128));


    }
  }
} 




int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImgOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);



   OCTET *ImgIn, *ImgOut;

   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   /*float max=0.;
   float min=0.;

   for(int x=0; x+8<=nW; x+=8){
    for (int y = 0; y+8<=nH; y+=8)
    {
      float m = dct(ImgIn, ImgMap, nH, nW, x, y);
      if (m<min) min = m;
      if (m>max) max = m;
    }
   }

   printf("dct max = %.4f\ndct min = %.4f\n", max, min);*/
    for(int x=0; x+8<=nH; x+=8){
      for (int y = 0; y+8<=nW; y+=8)
      {
        dec(ImgIn, ImgOut, nH, nW, x, y);
      }
    }
    //dec(ImgIn, ImgOut, nH, nW, 0, 0);


   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);


   free(ImgIn); free(ImgOut);

   return 1;
}