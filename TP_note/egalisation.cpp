//egalisation.cpp : augmente les contraste d'une image pgm par égalisation

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

int main(int argc, char const *argv[])
{
	char cNomImgLue[250], cNomImgEcrite[250];
	long double Niv[256];  //un vecteur des 256 niveaux de gris possible dans lequel on va sommer le nombre de pixels y correspondant
	int nHeight, nWidth, nTaille;

	if (argc != 3) 
     {
       printf("Usage: Image.pgm HistoOut.dat \n"); 
       exit (1) ;
     }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    

    OCTET *ImgIn, *ImgOut; //on définit le tableau dans lequel on va lire l'image

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nHeight, &nWidth);
    nTaille = nHeight * nWidth;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nHeight * nWidth);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int i = 0; i<256; i++)
    {
    	Niv[i]=0;
    }
	
	long double nbPixels=0.;

    //On somme les pixels dans Niv suivant leur niveau de gris
    for (int i = 0; i<nTaille; i++)
    {
    	Niv[ImgIn[i]]++;
		nbPixels++;
    }

	//printf("%Lf", nbPixels);
    for (int n = 0; n<256; n++)
    {
    	Niv[n]/=nbPixels;
    }
    for (int n = 1; n<256; n++)
    {
    	Niv[n]+=Niv[n-1];
    }

	for (int i = 0; i<nTaille; i++)
    {
    	ImgOut[i] = Niv[ImgIn[i]]*255.;
    }

	ecrire_image_pgm(cNomImgEcrite, ImgOut,  nHeight, nWidth);




	return 0;
}
