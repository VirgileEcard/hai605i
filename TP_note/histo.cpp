//histo.cpp produit un histogramme en .dat des niveaux de gris d'une image pgm

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char const *argv[])
{
	char cNomImgLue[250], cNomHisto[250];
	int Niv[256];  //un vecteur des 256 niveaux de gris possible dans lequel on va sommer le nombre de pixels y correspondant
	int nHeight, nWidth, nTaille;

	if (argc != 3) 
     {
       printf("Usage: Image.pgm HistoOut.dat \n"); 
       exit (1) ;
     }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomHisto);
    

    OCTET *ImgIn; //on définit le tableau dans lequel on va lire l'image

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nHeight, &nWidth);
    nTaille = nHeight * nWidth;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nHeight * nWidth);

    for (int i = 0; i<256; i++)
    {
    	Niv[i]=0;
    }


    //On somme les pixels dans Niv suivant leur niveau de gris
    for (int i = 0; i<nTaille; i++)
    {
    	Niv[ImgIn[i]]++;
    }


   //On écrit Niv dans un fichier .dat pour pouvoir l'utiliser avec GNUplot
   FILE *f_plot;

   if( (f_plot = fopen(cNomHisto, "wb")) == NULL)
      {
	 printf("\nPas d'acces en ecriture sur le fichier %s \n", cNomHisto);
	 exit(EXIT_FAILURE);
      }

   for (int i = 0; i<256; i++)
    {
    	fprintf(f_plot, "%d  %d\n", i, Niv[i] );
    }


	 fclose(f_plot);




	return 0;
}