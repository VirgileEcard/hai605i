// err_moy.cpp : calcule l'erreur moyenne entre deux images

#include <stdio.h>
#include "image_ppm.h"



int main(int argc, char* argv[])
{
  char cNomImgLue1[250], cNomImgLue2[250];
  int nH, nW, nTaille;

    
  if (argc != 3) 
     {
       printf("Usage: ImageIn1.pgm ImageIn2.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue1) ;
   sscanf (argv[2],"%s",cNomImgLue2) ;

   OCTET *ImgIn1, *ImgIn2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn1, OCTET, nTaille);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);

   double err=0;	
   for (int i=0; i < nTaille; i++){
    err+=abs(ImgIn1[i]-ImgIn2[i]);    
   }

   printf("La difference moyenne entre %s et %s est de %.2f\n", cNomImgLue1, cNomImgLue2, (float)err/(float)nTaille);

   free(ImgIn1); free(ImgIn2);

   return 1;
}