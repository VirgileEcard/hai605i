// bruit3.cpp : chaque pixel est échangé avec un pixel choisi dans les 5 pixels alentours
// on interprète cela comme un choix dans un carré de 5 pixels de côté

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "image_ppm.h"


int bruit(OCTET * Img, int nW, int nH, int x, int y){
    while(true){
      int dx=(rand()%5)-2;
      int dy=(rand()%5)-2;
      if((((x+dx) >= 0)&((x + dx) < nW))&
        (((y+dy) >= 0)&((y + dy) < nH))) {
        return Img[(x+dx)*nW+(y+dy)];
      }
    }
}



int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  time_t t;
  srand((unsigned) time(&t));
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;


   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[x*nW+y]=bruit(ImgIn, nW, nH, x, y);
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}