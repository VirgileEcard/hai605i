// bruit1.cpp : applique un bruitage "poivre et sel" à une image

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "image_ppm.h"


int bruit(OCTET * Img, int n){
  int i;


  if ((i=rand()%100)<20){
    return (rand()%256);
  }

  else{
    return Img[n];
  }

}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  time_t t;
  srand((unsigned) time(&t));

  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;

   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int n=0; n<nTaille; n++){
      ImgOut[n]=bruit(ImgIn, n);
      //printf("%d\n", ImgOut[n]);
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}