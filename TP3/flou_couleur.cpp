// flou_couleur.cpp : floute une image en couleurs

#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.ppm ImageOut.ppm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);


   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   int nW3=nW*3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille3);

  for (int i=0; i<nH; i++){
    ImgOut[i*3*nW]=ImgIn[i*3*nW];
    ImgOut[(i*3*nW)+1]=ImgIn[(i*3*nW)+1];
    ImgOut[(i*3*nW)+2]=ImgIn[(i*3*nW)+2];
    ImgOut[(i*3*nW)+(3*(nW-1))]=ImgIn[(i*3*nW)+(3*(nW-1))];
    ImgOut[(i*3*nW)+(3*(nW-1))+1]=ImgIn[(i*3*nW)+(3*(nW-1))+1];
    ImgOut[(i*3*nW)+(3*(nW-1))+2]=ImgIn[(i*3*nW)+(3*(nW-1))+2];

  }
  for (int j=0; j<3*nW; j+=3){
    ImgOut[j]=ImgIn[j];
    ImgOut[j+1]=ImgIn[j+1];
    ImgOut[j+2]=ImgIn[j+2];
    ImgOut[j+(3*nW*(nH-1))]=ImgIn[j+(3*nW*(nH-1))];
    ImgOut[j+1+(3*nW*(nH-1))]=ImgIn[j+1+(3*nW*(nH-1))];
    ImgOut[j+2+(3*nW*(nH-1))]=ImgIn[j+2+(3*nW*(nH-1))];
  }
  
  
  for (int i=1; i < nH-1; i++){
    for (int j=3; j < (nW*3)-3; j+=3)
      {
        int somG = ImgIn[i*nW3+j] + ImgIn[(i+1)*nW3+j] + ImgIn[i*nW3+(j+3)] + ImgIn[(i-1)*nW3+j] + ImgIn[i*nW3+(j-3)] +
        ImgIn[(i+1)*nW3+(j+3)] + ImgIn[(i+1)*nW3+(j-3)] + ImgIn[(i-1)*nW3+(j+3)] + ImgIn[(i-1)*nW3+(j-3)];
        ImgOut[i*nW3+j]=somG/9;

        int somB = ImgIn[i*nW3+j+1] + ImgIn[(i+1)*nW3+j+1] + ImgIn[i*nW3+(j+3)+1] + ImgIn[(i-1)*nW3+j+1] + ImgIn[i*nW3+(j-3)+1] +
        ImgIn[(i+1)*nW3+(j+3)+1] + ImgIn[(i+1)*nW3+(j-3)+1] + ImgIn[(i-1)*nW3+(j+3)+1] + ImgIn[(i-1)*nW3+(j-3)+1];
        ImgOut[i*nW3+j+1]=somB/9;

        int somR = ImgIn[i*nW3+j+2] + ImgIn[(i+1)*nW3+j+2] + ImgIn[i*nW3+(j+3)+2] + ImgIn[(i-3)*nW3+j+2] + ImgIn[i*nW3+(j-3)+2] +
        ImgIn[(i+1)*nW3+(j+3)+2] + ImgIn[(i+1)*nW3+(j-3)+2] + ImgIn[(i-1)*nW3+(j+3)+2] + ImgIn[(i-1)*nW3+(j-3)+2];
        ImgOut[i*nW3+j+2]=somR/9;
      }
  }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn);
   return 1;
}