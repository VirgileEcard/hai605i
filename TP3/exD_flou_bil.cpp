// flou_bil.cpp : applique un flou bilatéral à une image en niveaux de gris

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"


float gauss(int x, float sigma){
  return ((1/(2.507*sigma))*pow(2.72, -((x*x)/(2*sigma*sigma))));
}


/***************Fonction d'application du masque sur le pixel****************/
int appliMasque(OCTET * Img, int nW, int nH, int x, int y, float * Masque, int M, float sigInt){
  float som = 0.;
  float div = 0.;

  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      if (Masque[((dx+M/2)*M)+(dy+M/2)]==0) continue;
      if(((((x+dx) >= 0)&((x + dx) < nW))&
        (((y+dy) >= 0)&((y + dy) < nH)))&
        (!((dx==0)&(dy==0)))) {
        //On applique le masque gaussien (en distance) et on fait le produit par une gaussienne
        //appliquée à la différence d'intensité des pixels
        som+=Img[((x+dx)*nW+(y+dy))]*gauss(abs(Img[(x*nW)+y]-Img[((x+dx)*nW+(y+dy))]), sigInt)*Masque[((dx+M/2)*M)+(dy+M/2)];
        div+=gauss(abs(Img[(x*nW)+y]-Img[((x+dx)*nW+(y+dy))]), sigInt)*Masque[((dx+M/2)*M)+(dy+M/2)];
      }
    }
  }

  return (som/div);
}


float gauss2D(int x, int y, float sigma){
  return ((1/(2*3.1416*sigma*sigma))*pow(2.72, -((x*x)+(y*y))/(2*sigma*sigma)));
}

/*************Fonction de création d'un masque de type "disque avec des coefficients dépendants de la distance"
 *************suivant une gaussienne en 2D  ************/
void creerMasqueDisque(float * Masque, int M, float sigma){
  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
        Masque[((dx+(M/2))*M)+(dy+(M/2))]=gauss2D(dx, dy, sigma);
    }
  }
}


/*************Fonction d'affichage du masque pour débugger**********/
void afficheMasque(float * Masque, int M){
  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      printf("%.8f ", Masque[((dx+M/2)*M)+(dy+M/2)]);
    }
    printf("\n");
  }
}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nTailleFlou;
  float sigInt, sigSpa;
  
  if (argc != 6) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm TailleFlou SigmaIntensite SigmaSpatial\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[3],"%d", &nTailleFlou);
   sscanf (argv[4],"%f", &sigInt);
   sscanf (argv[5],"%f", &sigSpa);



   OCTET *ImgIn, *ImgOut;

   int M = (nTailleFlou*2)+1;
   float Masque[M*M];

   creerMasqueDisque(Masque, M, sigSpa);
   //afficheMasque(Masque, M);


   sscanf (argv[2],"%s",cNomImgEcrite);
   
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[x*nW+y]=appliMasque(ImgIn, nW, nH, x, y, Masque, M, sigInt);
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}