// flou_coef.cpp : floute une image en niveau de gris, les coef des voisins dépendant de leur distance au pixel traité

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

/***************Fonction dapplication du masque sur le pixel****************/
int appliMasque(OCTET * Img, int nW, int nH, int x, int y, float * Masque, int M){
  float som = 0.;
  float div = 0.;

  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      if (Masque[((dx+M/2)*M)+(dy+M/2)]==0) continue;
      if((((x+dx) >= 0)&((x + dx) < nW))&(((y+dy) >= 0)&((y + dy) < nH))){
        som+=Img[((x+dx)*nW+(y+dy))]*Masque[((dx+M/2)*M)+(dy+M/2)];
        div+=Masque[((dx+M/2)*M)+(dy+M/2)];
      }
    }
  }

  return (som/div);
}

/*************Fonction de création d'un masque de type "disque avec des coefficients dépendants de la distance"************/
void creerMasqueDisque(float * Masque, int M){
  for(int dx = 0; dx<=M; dx++){
    for(int dy = 0; dy<=M; dy++){
      //printf("pour la case %d  %d, la distance est %f\n", dx, dy, sqrt(pow(((M/2)-dx), 2) + pow(((M/2)-dy), 2)));
      float dist = (sqrt(pow(((M/2)-dx), 2) + pow(((M/2)-dy), 2)));
      if(dist <= (M/2)) {
        Masque[(dx*M)+dy]=(M/2)-dist;
      }
      else Masque[(dx*M)+dy]=0.;
    }
  }
}


/*************Fonction d'affichage du masque pour débugger**********/
void afficheMasque(float * Masque, int M){
  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      printf("%.2f ", Masque[((dx+M/2)*M)+(dy+M/2)]);
    }
    printf("\n");
  }
}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nTailleFlou;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm TailleFlou\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[3],"%d", &nTailleFlou);

   OCTET *ImgIn, *ImgOut;

   int M = (nTailleFlou*2)+1;
   float Masque[M*M];

   creerMasqueDisque(Masque, M);
   afficheMasque(Masque, M);


   sscanf (argv[2],"%s",cNomImgEcrite);
   
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[x*nW+y]=appliMasque(ImgIn, nW, nH, x, y, Masque, M);
      //printf("%d\n", ImgOut[x*nW+y]);
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}