// flou_contour.cpp : floute une image en niveau de gris en tentant de conserver les contours
//On donne à chaque voisin un coef inversement proportionnel à sa différence de valeur avec le pixel traité
//ici simplement l'inverse de la distance.

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"


int appliMasque(OCTET * Img, int nW, int nH, int x, int y, float * Masque, int M){
  float som = 0.;
  float div = 0.;
  int val;

  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      if((((x+dx) >= 0)&((x + dx) < nW))&(((y+dy) >= 0)&((y + dy) < nH))){
        if((dx==0)&&(dy==0)) {
          som+=(float)Img[((x+dx)*nW+(y+dy))];
          div++;
        }
        else{
          val= abs(Img[((x+dx)*nW+(y+dy))]-Img[x*nW+y]);
          if(!val){ //on évite la division par zéro
            som+=(float)Img[((x+dx)*nW+(y+dy))];
            div++;
          }
          else{
            som+=((float)Img[((x+dx)*nW+(y+dy))]/(float)val);
            div+=(1/(float)val);
          }
        }
      }
    }
  }

  return (int)(som/div);
}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nTailleFlou;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm TailleFlou\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d", &nTailleFlou);

   OCTET *ImgIn, *ImgOut;

   int M = (nTailleFlou*2)+1;
   float Masque[M*M];

   for (int i = 0; i < M*M; i++)
   {
     Masque[i]=1.;
   }


   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[x*nW+y]=appliMasque(ImgIn, nW, nH, x, y, Masque, M);
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}