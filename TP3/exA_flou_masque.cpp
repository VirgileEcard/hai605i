// flou.cpp : floute une image en niveau de gris, la taille du flou étant donnée par l'utilisateur

#include <stdio.h>
#include "image_ppm.h"


int appliMasque(OCTET * Img, int nW, int nH, int x, int y, float * Masque, int M){
  int som = 0;
  int div = 0;

  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      if((((x+dx) >= 0)&((x + dx) < nW))&(((y+dy) >= 0)&((y + dy) < nH))){
        som+=Img[((x+dx)*nW+(y+dy))];
        div+=Masque[((dx+M/2)*M)+(dy+M/2)];
      }
    }
  }

  return (som/div);
}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nTailleFlou;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm TailleFlou\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d", &nTailleFlou);

   OCTET *ImgIn, *ImgOut;

   int M = (nTailleFlou*2)+1;
   float Masque[M*M];

   for (int i = 0; i < M*M; i++)
   {
     Masque[i]=1.;
   }


   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[x*nW+y]=appliMasque(ImgIn, nW, nH, x, y, Masque, M);
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}