// flou_med.cpp : applique un flou médian à une image en niveaux de gris

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"


/********algo de tri rapide pour le calcul de la médiane************/
int part(int * Tab, int deb, int fin){
  int pivot = Tab[fin];
  int cnt = deb;

  for (int i = deb; i < fin; i++)
  {
    if(Tab[i]<pivot){
      int temp = Tab[cnt];
      Tab[cnt]=Tab[i];
      Tab[i]=temp;
      cnt++;
    }
  }
  Tab[fin] = Tab[cnt];
  Tab[cnt] = pivot;

  return cnt;
}

void tri(int * Tab, int deb, int fin){
  if(deb<fin){
    int pivot = part(Tab, deb, fin);
    tri(Tab, deb, pivot-1);
    tri(Tab, pivot+1, fin);
  }
}


/***************Fonction de calcul de la médiane des pixels du disque****************/
int med(OCTET * Img, int nW, int nH, int x, int y, float * Masque, int M){
  int nb = 0;
  int Val[M*M];

  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      if (Masque[((dx+M/2)*M)+(dy+M/2)]==0) {
        continue;
      }
      if((((x+dx) >= 0)&((x + dx) < nW))&(((y+dy) >= 0)&((y + dy) < nH))){
        Val[nb++]=Img[(x+dx)*nW+(y+dy)];
      }
    }
  }

  tri(Val, 0, nb-1);

  return Val[nb/2];
}

/*************Fonction de création d'un masque de type "disque de 1"************/
void creerMasqueDisque(float * Masque, int M){
  for(int dx = 0; dx<=M; dx++){
    for(int dy = 0; dy<=M; dy++){
      //printf("pour la case %d  %d, la distance est %f\n", dx, dy, sqrt(pow(((M/2)-dx), 2) + pow(((M/2)-dy), 2)));
      if(sqrt(pow(((M/2)-dx), 2) + pow(((M/2)-dy), 2)) <= (M/2)){
        Masque[(dx*M)+dy]=1.;
      }
      else Masque[(dx*M)+dy]=0.;
    }
  }
}


/*************Fonction d'affichage du masque pour débugger**********/
void afficheMasque(float * Masque, int M){
  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      printf("%.0f ", Masque[((dx+M/2)*M)+(dy+M/2)]);
    }
    printf("\n");
  }
}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nTailleFlou;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm TailleFlou\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[3],"%d", &nTailleFlou);

   OCTET *ImgIn, *ImgOut;

   int M = (nTailleFlou*2)+1;
   float Masque[M*M];

   //On utilise un masque "disque" pour calculer la médiane des pixels à une certaine distance du pixel à traiter
   creerMasqueDisque(Masque, M);

   sscanf (argv[2],"%s",cNomImgEcrite);

   //afficheMasque(Masque, M);



   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);





   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[x*nW+y]=med(ImgIn, nW, nH, x, y, Masque, M);
      //printf("med = %d\n", ImgOut[x*nW+y]);
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}