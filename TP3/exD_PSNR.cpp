// PSNR.cpp : calcule le Peak Signal to Noise Ratio entre deux images

#include <stdio.h>
#include "image_ppm.h"
#include <math.h>



int main(int argc, char* argv[])
{
  char cNomImgLue1[250], cNomImgLue2[250];
  int nH, nW, nTaille;

    
  if (argc != 3) 
     {
       printf("Usage: ImageIn1.pgm ImageIn2.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue1) ;
   sscanf (argv[2],"%s",cNomImgLue2) ;

   OCTET *ImgIn1, *ImgIn2;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn1, OCTET, nTaille);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);


   //on calcule d'abord l'erreur moyenne
   double err=0;	
   for (int i=0; i < nTaille; i++){
    err+=(abs(ImgIn1[i]-ImgIn2[i])*abs(ImgIn1[i]-ImgIn2[i]));    
   }

   err/=nTaille;


   float psnr;

   psnr = (20*log10(255))-(10*log10(err));

   printf("Le PSNR entre %s et %s est de %.2f dB", cNomImgLue1, cNomImgLue2, psnr);


   free(ImgIn1); free(ImgIn2);

   return 1;
}