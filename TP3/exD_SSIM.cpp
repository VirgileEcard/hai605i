// SSIM.cpp : calcule la Structural SIMilarity entre deux images par blocs de 8*8
// et produit une "SSIM map"
#include <stdio.h>
#include <math.h>
#include "image_ppm.h"


float ssim(OCTET * Img1, OCTET * Img2, OCTET * ImgOut, int nW, int nH, int x, int y){
  float som1=0.;
  float som2=0.;
  float somC=0.;
  float moy1, moy2, var1, var2, cov;
  float c1 = pow((0.01*255), 2);
  float c2 = pow((0.03*255), 2);

  /******calcul des moyennes*********/
  for(int dx = 0; dx<8; dx++){
    for(int dy = 0; dy<8; dy++){
      som1+=Img1[(x+dx)*nW+(y+dy)];
      som2+=Img2[(x+dx)*nW+(y+dy)];
    }
  }
  moy1=(som1/64.);
  moy2=(som2/64.);

  som1=0;
  som2=0;



/************calcul des variances**********/
  for(int dx = 0; dx<8; dx++){
    for(int dy = 0; dy<8; dy++){
      som1+=(pow(abs(moy1-(float)Img1[(x+dx)*nW+(y+dy)]), 2));
      som2+=(pow(abs(moy2-(float)Img2[(x+dx)*nW+(y+dy)]), 2));

      somC+=(abs(moy1-(float)Img1[(x+dx)*nW+(y+dy)])*abs(moy2-(float)Img2[(x+dx)*nW+(y+dy)]));

    }
  }
  var1=(som1/64.);
  var2=(som2/64.);
  cov=(somC/64.);

/*********formule du SSIM************/

  float s = ( ((2 * moy1 * moy2) + c1) * ((2 * cov) + c2) / ((pow(moy1, 2) + pow(moy2, 2) + c1) * (var1 + var2 + c2)) );

  /*********remplissage de la carte de SSIM*******/
  for(int dx = 0; dx<8; dx++){
    for(int dy = 0; dy<8; dy++){
      ImgOut[(x+dx)*nW+(y+dy)]=(OCTET)(s*255);
    }
  }

  return s;

}


int main(int argc, char* argv[])
{
  char cNomImgLue1[250], cNomImgLue2[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 4) 
     {
       printf("Usage: ImageIn1.pgm ImageIn2.pgm ImgOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue1) ;
   sscanf (argv[2],"%s",cNomImgLue2);
   sscanf (argv[3],"%s",cNomImgEcrite);


   OCTET *ImgIn1, *ImgIn2, *ImgOut;

   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn1, OCTET, nTaille);
   lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   float somS=0.;
   float nbBlocs=0.;

	
   for(int x=0; x+8<nW; x+=8){
    for (int y = 0; y+8<nH; y+=8)
    {
      float s = ssim(ImgIn1, ImgIn2, ImgOut, nW, nH, x, y);
      somS += s;
      nbBlocs++;
    }
   }

   printf("Le SSIM moyen entre les images est %f", (somS/nbBlocs));

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);


   free(ImgIn1); free(ImgIn2); free(ImgOut);

   return 1;
}