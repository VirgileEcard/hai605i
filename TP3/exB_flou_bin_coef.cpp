// flou_bin.cpp : applique un flou sur une partie d'une image en niveau de gris
// selon la valeur du pixel dans une image binaire passée en paramètre
//version optimisée, où on donne un coef à chaque pixel selon sa valeur dans l'image binaire

#include <stdio.h>
#include "image_ppm.h"


int appliMasque(OCTET * Img, OCTET * ImgB, int nW, int nH, int x, int y, float * Masque, int M){
  int som = 0;
  int div = 0;
  //printf("pixel %d, %d \n", x, y);


  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      if((((x+dx) >= 0)&((x + dx) < nW))&(((y+dy) >= 0)&((y + dy) < nH))){
        if((dx==0)&&(dy==0)) {
          som+=Img[((x+dx)*nW+(y+dy))];
          div+=Masque[((dx+M/2)*M)+(dy+M/2)];
        }
        else{
        som+=(Img[((x+dx)*nW+(y+dy))] * (1-ImgB[((x+dx)*nW+(y+dy))]/255));
        div+=(Masque[((dx+M/2)*M)+(dy+M/2)] * (1-(ImgB[((x+dx)*nW+(y+dy))]/255)));
        }
      }
    }
  }

  return som/div;
}


int main(int argc, char* argv[])
{
  char cNomImgLue1[250], cNomImgLue2[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nTailleFlou;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageBinaire.pgm ImageOut.pgm TailleFlou\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue1) ;
   sscanf (argv[2],"%s",cNomImgLue2) ;
   sscanf (argv[3],"%s",cNomImgEcrite);
   sscanf (argv[4],"%d", &nTailleFlou);

   OCTET *ImgIn1, *ImgIn2, *ImgOut;

   int M = (nTailleFlou*2)+1;
   float Masque[M*M];

   for (int i = 0; i < M*M; i++)
   {
     Masque[i]=1.;
   }


   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue1, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn1, OCTET, nTaille);
   lire_image_pgm(cNomImgLue1, ImgIn1, nH * nW);
   allocation_tableau(ImgIn2, OCTET, nTaille);
   lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[x*nW+y]=appliMasque(ImgIn1, ImgIn2, nW, nH, x, y, Masque, M);
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn1); free(ImgIn2); free(ImgOut);

   return 1;
}