// bruit2.cpp : applique un bruitage à une image en niveaux de gris 
// en donnant la valeur à un pixel d'un de ses voisins avec une probabilité de 20% 5 fois

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "image_ppm.h"


int bruit(OCTET * Img, int nW, int nH, int x, int y){
  for(int i=0; i<5; i++){
    if((rand()%100)<20){
      while(true){
        int dx=(rand()%3)-1;
        int dy=(rand()%3)-1;
        if((!((dx==0)&(dy==0)))&
          (((x+dx) >= 0)&((x + dx) < nW))&
          (((y+dy) >= 0)&((y + dy) < nH))){
          return Img[(x+dx)*nW+(y+dy)];
        }
      }
    }
  }

  return Img[x*nW+y];

}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  time_t t;
  srand((unsigned) time(&t));
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;


   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[x*nW+y]=bruit(ImgIn, nW, nH, x, y);
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}