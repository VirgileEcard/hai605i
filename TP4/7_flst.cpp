// flst.cpp : binarise une image en utilisant l'algorithme de Floyd-Steinberg

#include <stdio.h>
#include "image_ppm.h"


int appliMasque(OCTET * Img, int nW, int nH, int x, int y, float * Masque, int M){
  int som = 0;
  int div = 0;

  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      if((((x+dx) >= 0)&((x + dx) < nW))&(((y+dy) >= 0)&((y + dy) < nH))){
        som+=Img[((x+dx)*nW+(y+dy))];
        div+=Masque[((dx+M/2)*M)+(dy+M/2)];
      }
    }
  }

  return (som/div);
}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  

   float ImgOut[nTaille];

   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

   for(int i = 0; i<nTaille; i++){
    ImgOut[i]=(float)ImgIn[i];
   }
	

   for(int x=0; x<nW-1; x++){
    for (int y = 0; y<nH-1; y++)
    {
      int ancienPixel = ImgOut[x*nW+y];
      ImgOut[x*nW+y] = (ancienPixel<128)? 0. : 255.;
      float err = ancienPixel - ImgOut[x*nW+y];
      ImgOut[x*nW+(y+1)]+=((7./16.)*err);
      ImgOut[(x+1)*nW+(y-1)]+=((3./16.)*err);
      ImgOut[(x+1)*nW+y]+=((5./16.)*err);
      ImgOut[(x+1)*nW+(y+1)]+=((1./16.)*err);
    }
   }

   for(int i = 0; i<nTaille; i++){
    ImgIn[i]=(OCTET)ImgOut[i];
   }

   ecrire_image_pgm(cNomImgEcrite, ImgIn,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}