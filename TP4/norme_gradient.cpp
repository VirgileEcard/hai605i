// norme_gradient.cpp : produit la carte des normes des gradients pour une image pgm

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  float gradV, gradH, grad;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   //float gradMax=0.;
	
 for (int i=0; i < nH-1; i++){
   for (int j=0; j < nW-1; j++)
     {
       gradV = ImgIn[i*nW+(j+1)] - ImgIn[i*nW+j];
       gradH = ImgIn[(i+1)*nW+j] - ImgIn[i*nW+j];

       grad = sqrt((float)pow(gradH, 2) + (float)pow(gradV, 2));
       //if(grad>gradMax) gradMax = grad;
       ImgOut[i*nW+j] = grad;

     }
   }

    //printf("grad max de %s : %f", cNomImgLue, gradMax);


   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}