// grad_ppm.cpp : produit une carte des gradients en ppm

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  OCTET gradV, gradH;
  //float grad;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.ppm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut, *ImgR, *ImgV, *ImgB;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;

  int nTaille3 = nTaille * 3;

  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgR, OCTET, nTaille);
   allocation_tableau(ImgV, OCTET, nTaille);
   allocation_tableau(ImgB, OCTET, nTaille);


   allocation_tableau(ImgOut, OCTET, nTaille3);
	
 for (int i=0; i < nH - 1; i++){
    //printf("%d\n", i);
   for (int j=0; j < nW-1; j++){
    //printf("%d\n", j);
       gradV = (ImgIn[i*nW+(j+1)] - ImgIn[i*nW+j]);
       gradH = (ImgIn[(i+1)*nW+j] - ImgIn[i*nW+j]);
       //printf("%d\n", gradV);
       //grad = sqrt((float)pow(gradH, 2) + (float)pow(gradV, 2));
       ImgR[i*nW+j] = /*gradH+128;*/MIN(gradH+128, 255);
       ImgV[i*nW+j] = /*gradV+128;*/MIN(gradV+128, 255);
       ImgB[i*nW+j] = 255;
     }
   }

    for (int i=0; i < nTaille3; i+=3)
     {
       ImgOut[i]=ImgR[i/3];
       ImgOut[i+1]=ImgV[i/3]; 
       ImgOut[i+2]=ImgB[i/3];
     }

   ecrire_image_ppm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
   free(ImgR); free(ImgV); free(ImgB);

   return 1;
}