// ccl.cpp : labellise les zones d'une image binaire et produit une image où les labels sont
// représentés par différents niveaux de gris 

#include <stdio.h>
#include "image_ppm.h"
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

int find(int x, int* Tab){
  while(x!=Tab[x]){
    x=Tab[x];
  }

  return x;
}

void unionEq(int x, int y, int* Tab){
  Tab[find(y, Tab)]=find(x, Tab);
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);


   OCTET *ImgIn, *ImgOut;
   int *ImgLab;

   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
   
   int LabEq[nTaille];
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

   ImgLab = (int*)calloc(nTaille, sizeof(int));


  for(int i = 0; i<nTaille; i++){
    ImgLab[i]=-1;
    LabEq[i] = i;
  }

  int labCourant = 0;

  for (int i = 0; i < nH-1; i++) {
    for(int j = 0; j < nW-1; j++) {
      //printf("%d %d\n", i, j);
      //int nouvLab = 0;
      if(ImgIn[i*nW+j]==0){
        //if((i>0)&&(j>0)){
          int gauche = 1;
          int haut = 1;
          if (i==0) haut = 0;
          else if(ImgLab[(i-1)*nW+j]==-1) haut = 0;
          if (j==0) gauche = 0;
          else if(ImgLab[i*nW+(j-1)]==-1) gauche = 0;

          if((!(gauche)) && (!(haut))) {//((i==0) | (ImgLab[(i-1)*nW+j]==-1)) && ((j==0) | (ImgLab[i*nW+(j-1)]==-1))){
            //printf("cndtion 1 entrée\n");
            labCourant++;
            ImgLab[i*nW+j]=labCourant;
          }
          else if( gauche && (!(haut)) ){
            //printf("cndtion 3 entrée\n");

            ImgLab[i*nW+j] = find(ImgLab[i*nW+(j-1)], LabEq);
          }


          else if( haut && (!(gauche)) ){
            //printf("cndtion 2 entrée\n");

            ImgLab[i*nW+j] = find(ImgLab[(i-1)*nW+j], LabEq);
          }

          else if(ImgLab[(i-1)*nW+j]==ImgLab[i*nW+(j-1)]){
            ImgLab[i*nW+j] = find(ImgLab[(i-1)*nW+j], LabEq);
          }

          else{
            //printf("cndtion 4 entrée\n");
            int min = MIN(ImgLab[i*nW+(j-1)], ImgLab[(i-1)*nW+j]);
            int max = MAX(ImgLab[i*nW+(j-1)], ImgLab[(i-1)*nW+j]);
            unionEq(min, max, LabEq);
            ImgLab[i*nW+j] = find(min, LabEq);
          }
        }
      }
    }

  //printf("On a %d labels\n", labCourant);

  for (int i = 0; i < nH; i++){
    //printf("%d\n", i);
    for(int j = 0; j < nW; j++){
      if(ImgIn[i*nW+j]==0){
        ImgLab[i*nW+j]=find(ImgLab[i*nW+j], LabEq);
        //printf("pixel %d %d, label %d\n", i, j, ImgLab[i*nW+j]);

      }
    }
  }



  int histoLab[labCourant+1];
  int labDif = 0;


  for(int k=0; k<=labCourant; k++){
    histoLab[k]=0;
  }

  for(int k=0; k<=nTaille; k++){
    histoLab[ImgLab[k]]++;
  }

  /*for(int k=0; k<=labCourant-1; k++){
    printf("%d ~ %d\n", k, LabEq[k]);
  }*/

  for(int k=0; k<=labCourant; k++){
    if(histoLab[k]){
      /*for(int n=0; n<=labCourant; n++){
        if(LabEq[n]==k){
          LabEq[n]=labDif;
        }
      }*/
      labDif++;
    }
  }
  
  for(int n=0; n<nTaille; n++){
    if (ImgLab[n]==-1) ImgOut[n]=0;
    else ImgOut[n]= (OCTET)(255-((ImgLab[n])*(255./labDif)));
  }

  ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
  free(ImgIn); free(ImgOut); free(ImgLab); free(LabEq);

  return 1;
}