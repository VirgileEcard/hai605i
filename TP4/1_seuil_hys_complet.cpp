// seuil_hys_complet.cpp : Seuille une image en niveau de gris par hystérèse complète

#include <stdio.h>
#include "image_ppm.h"


int aVoisinBlanc(int i, int j, int nH, int nW, OCTET * Img){
int b = 0;

  for (int dx = -1; dx <= 1; dx++){
    for(int dy = -1; dy <= 1; dy++){
      if( (((i+dx)>=0)&&((j+dy)>=0)) && (!((dx==0)&&(dy==0))) ){
        if(Img[(i+dx)*nW+(j+dy)]==255) b = 1;
      }
    }
  }
  return b;
}

int tousVoisinsNoirs(int i, int j, int nH, int nW, OCTET * Img){
int b = 1;

  for (int dx = -1; dx <= 1; dx++){
    for(int dy = -1; dy <= 1; dy++){
      if(((i+dx)>=0) && ((j+dy)>=0) && (!((dx==0)&&(dy==0)))   )  {
        if(Img[(i+dx)*nW+(j+dy)]!=0) b = 0;
      }
    }
  }
  return b;
}



int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, SH, SB;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm SeuilBas SeuilHaut \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&SB);
   sscanf (argv[4],"%d",&SH);


   OCTET *ImgIn, *ImgOut, *ImgTemp;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
   allocation_tableau(ImgTemp, OCTET, nTaille);

	
  for (int n = 0; n < nTaille; n++)
  {
    if (ImgIn[n]<SB) ImgTemp[n]=0;
    else if(ImgIn[n]>SH) ImgTemp[n]=255;
    else ImgTemp[n]=ImgIn[n];
  }

  //int cpt=1;
  int pixelTraite;

  do{
    pixelTraite = 0;
    for (int i = 0; i < nH; i++){
      for(int j = 0; j < nW; j++){

        if (!((ImgTemp[i*nW+j]==0)|(ImgTemp[i*nW+j]==255))){
          if(aVoisinBlanc(i, j, nH, nW, ImgTemp)){
            ImgTemp[i*nW+j]=255;
            pixelTraite++;
          }
          else if(tousVoisinsNoirs(i, j, nH, nW, ImgTemp)){
            ImgTemp[i*nW+j]=0;
            pixelTraite++;
          }
        }
      }
    }
    //printf("Tour %d de la boucle, %d pixels traites\n", cpt++, pixelTraite);
  }while(pixelTraite);

  for (int n = 0; n < nTaille; n++)
  {
    if (ImgTemp[n]==255) ImgOut[n]=255;
    else ImgOut[n]=0;
  }



   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut); free(ImgTemp);

   return 1;
}