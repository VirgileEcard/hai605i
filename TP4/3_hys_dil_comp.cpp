// hys_dil_comp.cpp : Seuille une image en niveau de gris par hystérèse complète en utilisant la dilatation

#include <stdio.h>
#include "image_ppm.h"

int aVoisinBlanc(int i, int j, int nH, int nW, OCTET * Img){
int b = 0;

  for (int dx = -1; dx <= 1; dx++){
    for(int dy = -1; dy <= 1; dy++){
      if(((i+dx)>=0)&&((j+dy)>=0)){
        if(Img[(i+dx)*nW+(j+dy)]==255) b = 1;
      }
    }
  }
  return b;
}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, SH, SB;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm SeuilBas SeuilHaut \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);
   sscanf (argv[3],"%d",&SB);
   sscanf (argv[4],"%d",&SH);


   OCTET *ImgIn, *ImgSb, *ImgSh, *ImgShD;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgSb, OCTET, nTaille);
   allocation_tableau(ImgSh, OCTET, nTaille);
   allocation_tableau(ImgShD, OCTET, nTaille);

	/***On seuille ImgSb avec le seuil bas et ImgSh avec le seuil haut***/
  for (int i=0; i < nH; i++){
    for (int j=0; j < nW; j++){
      {
        if ( ImgIn[i*nW+j] < SB) ImgSb[i*nW+j]=0;
        else ImgSb[i*nW+j]=255;
        if ( ImgIn[i*nW+j] < SH) ImgSh[i*nW+j]=0;
        else ImgSh[i*nW+j]=255;
      }
    }
  }

  int pixelTraite;

  do{
    pixelTraite = 0;

    /***On dilate ImgSh dans ImgShD***/
    for (int i=0; i < nTaille; i++){
      ImgShD[i]=ImgSh[i];
    }

    for (int i=0; i < nH; i++){
      for (int j=0; j < nW; j++){
        if (aVoisinBlanc(i, j, nH, nW, ImgSh)) ImgShD[i*nW+j]=255;
      }
    }

    for(int i=0; i<nTaille; i++){
      if((ImgSb[i]==255) && (ImgShD[i]==255) && (!(ImgSh[i]==255))){
        ImgSh[i]=255;
        pixelTraite++;
      }
    }

  }while(pixelTraite);



   ecrire_image_pgm(cNomImgEcrite, ImgSh,  nH, nW);
   free(ImgIn); free(ImgSb); free(ImgSh); free(ImgShD);

   return 1;
}