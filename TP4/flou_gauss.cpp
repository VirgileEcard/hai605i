// flou_gauss.cpp : applique un flou gaussien à une image en niveau de gris

#include <stdio.h>
#include <math.h>
#include "image_ppm.h"

/***************Fonction d'application du masque sur le pixel****************/
int appliMasque(OCTET * Img, int nW, int nH, int x, int y, float * Masque, int M){
  float som = 0.;
  float div = 0.;

  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      if (Masque[((dx+M/2)*M)+(dy+M/2)]==0) continue;
      if((((x+dx) >= 0)&((x + dx) < nW))&(((y+dy) >= 0)&((y + dy) < nH))){
        som+=Img[((x+dx)*nW+(y+dy))]*Masque[((dx+M/2)*M)+(dy+M/2)];
        div+=Masque[((dx+M/2)*M)+(dy+M/2)];
      }
    }
  }

  return (som/div);
}

float gauss(int x, int y, float sigma){
  return ((1/(2*3.1416*sigma*sigma))*pow(2.72, -((x*x)+(y*y))/(2*sigma*sigma)));
}

/*************Fonction de création d'un masque de type "disque avec des coefficients dépendants de la distance"
 *************suivant une gaussienne en 2D (sigma donné en argument) ***********/
void creerMasqueDisque(float * Masque, int M, float sigma){
  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
        Masque[((dx+(M/2))*M)+(dy+(M/2))]=gauss(dx, dy, sigma);
    }
  }
}


/*************Fonction d'affichage du masque pour débugger**********/
void afficheMasque(float * Masque, int M){
  for(int dx = -(M/2); dx<=(M/2); dx++){
    for(int dy = -(M/2); dy<=(M/2); dy++){
      printf("%0.3f ", Masque[((dx+M/2)*M)+(dy+M/2)]);
    }
    printf("\n");
  }
}


int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille, nTailleFlou;
  float nSigma;
  
  if (argc != 5) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm TailleFlou Sigma\n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[3],"%d", &nTailleFlou);
   sscanf (argv[4],"%f", &nSigma);


   OCTET *ImgIn, *ImgOut;

   int M = (nTailleFlou*2)+1;
   float Masque[M*M];

   creerMasqueDisque(Masque, M, nSigma);
   afficheMasque(Masque, M);


   sscanf (argv[2],"%s",cNomImgEcrite);
   
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   for(int x=0; x<nW; x++){
    for (int y = 0; y<nH; y++)
    {
      ImgOut[x*nW+y]=appliMasque(ImgIn, nW, nH, x, y, Masque, M);
    }
   }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}